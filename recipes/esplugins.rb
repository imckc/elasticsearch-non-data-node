# Install ElasticSearch Plugin Head
script "install_plugin_es_head" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin -install mobz/elasticsearch-head
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/head") }
end

# Install ElasticSearch AWS S3 plugin
script "install_plugin_aws_cloud" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin -install elasticsearch/elasticsearch-cloud-aws/2.5.1
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/cloud-aws") }
end

# Install ElasticSearch Plugin Bigdesk
script "install_plugin_bigdesk" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin -install lukas-vlcek/bigdesk
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/bigdesk") }
end

# Install ElasticSearch Plugin Paramedic
script "install_plugin_paramedic" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin -install karmi/elasticsearch-paramedic
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/paramedic") }
end

